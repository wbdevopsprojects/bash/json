#!/bin/bash

# Сгенирировать https://www.mockaroo.com 3000 записей форматов json, cvs, xml
# Файлы доступен по адресу: https://gitlab.com/wbdevopsprojects/bash/json.git 

set -euxo pipefail

cvsfile=$1
xmlfile=$2
jsonfile=$3

# Преобразовать svc в json 
yq -o=json $cvsfile > cvsdata.json

# Преобразовать xml в json 
yq -o=json $xmlfile --xml-skip-proc-inst --xml-skip-directives | jq .[][] > xmldata.json

# Объединять файлы
jq -s 'add' $jsonfile cvsdata.json xmldata.json > final.json

# Валидировать json файл 
jsonlint final.json

# Форматировать  
jsonlint -F final.json

# Фильтр по имени 
yq -o=json '.[].first_name' final.json | grep -E --ignore-case 'k|a' | sort > names.txt

# Сгенирироват ID
cat names.txt | xargs -I % sh -c 'echo `uuidgen` %'  > finalNames.txt

# Удалить дубликаты
sort -k 2 -u finalNames.txt -o finalNames.txt

# Архивировать в zst формат
tar -I zstd -cvf finalNames.tar.zst finalNames.txt > /dev/null

rm -rf names.txt cvsdata.json final.json names.txt xmldata.json finalNames.txt